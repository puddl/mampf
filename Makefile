MAKEFLAGS += --always-make

_default: ruff mypy test

ruff:
	ruff check .

mypy:
	mypy .

test:
	pytest

dev-setup:
	./env/dev/virtualenv-create.sh

gunicorn:
	gunicorn mampf.deploy.wsgi --workers 20 -b 0.0.0.0:8000 # all interfaces for docker socat

runserver:
	./manage.py runserver 0.0.0.0:8000

benchmark:
	ab -A alice:wonderland -p share/form_data.txt -T application/x-www-form-urlencoded -c 50 -n 1000 localhost:8000/ab
