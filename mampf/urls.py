from django.contrib import admin
from django.urls import path
from mampf import views
from mampf.api import api

urlpatterns = [
    path('', views.index, name='index'),
    path("-/admin", admin.site.urls, name='admin'),
    path("", api.urls, name='mampfer'),
]
