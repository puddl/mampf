from django.shortcuts import render
from django.urls import reverse
from pydantic import BaseModel


class Link(BaseModel):
    name: str
    href: str


def index(request):

    xs = [
        Link(name='admin', href=reverse('admin:index')),
    ]
    example_url = f'{request.scheme}://alice:wonderland@{request.get_host()}/foo'
    return render(
        request,
        'mampf/index.html',
        {
            'example_url': example_url,
            'links': xs,
        }
    )
