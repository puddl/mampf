from django.contrib.auth.models import AbstractUser
from django.db import models
from django.template.defaultfilters import truncatechars


class User(AbstractUser):
    pass


class Raw(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name='created at')
    path = models.TextField()
    data = models.JSONField()

    def __str__(self):
        preview = truncatechars(str(self.data), 80)
        return f'{self.id} {preview}'
