from django.contrib.auth import authenticate
from mampf.models import Raw
from ninja import NinjaAPI
from ninja.security import HttpBasicAuth


class BasicAuth(HttpBasicAuth):
    def authenticate(self, request, username, password):
        user = authenticate(username=username, password=password)
        return user


api = NinjaAPI(auth=[BasicAuth()], csrf=False)


@api.post("/{path:path}")
def post_stuff(request, path: str):
    raw = Raw(user=request.auth, path=path, data=request.POST)
    raw.save()
    return {k: getattr(raw, k) for k in ['path', 'data']}
