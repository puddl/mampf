from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from mampf.models import Raw

admin.site.register(get_user_model(), UserAdmin)


@admin.register(Raw)
class RawAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'path',
        'data',
    )
    list_filter = (
        'user',
        'path',
    )
