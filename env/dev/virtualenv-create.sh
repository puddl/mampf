#!/bin/bash
set -euo pipefail

PYTHON_VERSION=3.11
VENV_NAME=mampf

if pyenv versions --skip-aliases | grep -qv $PYTHON_VERSION; then
  pyenv install $PYTHON_VERSION
fi
if pyenv versions | grep -qv $VENV_NAME; then
  pyenv virtualenv $PYTHON_VERSION $VENV_NAME
else
  echo "WARNING: $VENV_NAME already exists" >&2
fi
pyenv local $VENV_NAME
