#!/bin/bash
set -euo pipefail

MAMPF_DB_WRITE_USER=mampf
MAMPF_DB_WRITE_PASSWORD=Eihas5ohthee

cat <<EOF
POSTGRES_PASSWORD=aedaeg0aiJei
GF_SECURITY_ADMIN_PASSWORD=secret

MAMPF_DB_NAME=mampf
MAMPF_DB_WRITE_USER=${MAMPF_DB_WRITE_USER}
MAMPF_DB_WRITE_PASSWORD=${MAMPF_DB_WRITE_PASSWORD}
MAMPF_DB_READ_USER=grafana
MAMPF_DB_READ_PASSWORD=auqu7BooL3oh

DEBUG=true
SECRET_KEY=development
ALLOWED_HOSTS=*
DATABASE_URL=postgres://${MAMPF_DB_WRITE_USER}:${MAMPF_DB_WRITE_PASSWORD}@127.0.0.1:15432/mampf
STATIC_ROOT=/tmp/static
CSRF_TRUSTED_ORIGINS=https://mampf.0-main.de
EOF
