# generate mampf/fixtures/users.json
```
./manage.py createsuperuser  # dev

mkdir -p mampf/fixtures/
./manage.py dumpdata --natural-primary --natural-foreign core.User > mampf/fixtures/users.json
```
