# What?
Mampf eats your data [^mampfen].

[^mampfen]: "Mampfen" is colloquial German for "eating". To be more precise:
    Eating a lot fast.


# Usage
```
curl -F value=89 https://alice:wonderland@mampf.example.com/besucher
```

This results in a database record in the `mampf_raw` table:

| user  | created_at | path | data |
|-------|---|------|------|
| alice | 2024-04-23 09:34:33.137242+02 | besucher | { "value": 89 } |


# Users
- admin `dev:secret`
- unprivileged `alice:wonderland`
```
./manage.py loaddata users
```


# Analytics
To transform the above table into something more easily digestible, you might want to do the following
```sql
SELECT t,
    data->>'value' AS besucher
FROM mampf_raw
WHERE user = 'hallenbad'
AND path = 'besucher'
;
```

... or even create a continuous materialized view:
```sql
CREATE MATERIALIZED VIEW v_hallenbad_besucher WITH (timescaledb.continuous) AS
  SELECT (data->>'dt')::timestamptz AS dt, (data->>'count')::int as count
  FROM mampf_raw
  WHERE user_id = (
    SELECT id FROM mampf_user WHERE username = 'hallenbad'
  ) AND path = 'besucher';
```


# Bots
Mampf allows you to easily create new users.
The above URL was generated as follows:

- Admin > Create Bot
    - Name: hallenbad

Bots are Django users with basic auth credentials.
This is a concept similar to "Authorization Tokens".
Instead of the HTTP Header `Authorization: Bearer ...`, Mampf uses basic auth to KISS.


# Deploy
Create a `.env`. For a complete example run
```
./env/dev/generate_env_file.sh
```

Start DB and Grafana
```
docker-compose up -d
```

Set up static files directory and start Gunicorn.
```
sudo install -d -o felix -g caddy -m 750 /var/www/mampf-static

./manage.py collectstatic --no-input
gunicorn mampf.deploy.wsgi
```


Configure reverse proxy (caddy in this case)
```
cat <<'EOF' >> /etc/caddy/Caddyfile
https://mampf.example.com {
	import wildcard_tls
	import log_and_errors mampf.example.com
	reverse_proxy http://127.0.0.1:8000
	handle_path /static/* {
		root * /var/www/mampf-static
		file_server
	}
}
EOF
```
